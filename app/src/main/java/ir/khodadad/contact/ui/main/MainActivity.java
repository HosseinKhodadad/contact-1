package ir.khodadad.contact.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import ir.khodadad.contact.R;
import ir.khodadad.contact.data.local.db.AppDatabase;
import ir.khodadad.contact.data.local.db.model.ContactModel;
import ir.khodadad.contact.data.repository.ContactRepo;
import ir.khodadad.contact.ui.insert.InsertActivity;

public class MainActivity extends AppCompatActivity {

    ListRecyclerAdapter recyclerAdapter;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase database = AppDatabase.getInstance(this);
        ContactRepo repository = new ContactRepo(database);

        FloatingActionButton insert = findViewById(R.id.main_insert);

        insert.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, InsertActivity.class));
        });

        RecyclerView recyclerView = findViewById(R.id.main_recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        repository.getListData().subscribe(list -> {
            recyclerAdapter = new ListRecyclerAdapter(this, list);

            runOnUiThread(() -> {
                recyclerView.setAdapter(recyclerAdapter);
            });
        });
    }
}