package ir.khodadad.contact.data.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ir.khodadad.contact.data.local.db.AppDatabase;
import ir.khodadad.contact.data.local.db.model.ContactModel;

public class ContactRepo {

    private AppDatabase database;

    public ContactRepo(AppDatabase database) {
        this.database = database;
    }

    public Completable insertContact(ContactModel model) {
        return Completable.fromAction(() -> database.contactDao().insertData(model))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Flowable<List<ContactModel>> getListData() {
        return database.contactDao().getData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Completable deleteContact(ContactModel model){
        return Completable.fromAction(() -> database.contactDao().deleteData(model))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
